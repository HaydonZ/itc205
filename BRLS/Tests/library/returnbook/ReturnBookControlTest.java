package library.returnbook;

import library.entities.ICalendar;
import library.entities.ILibrary;
import library.entities.ILoan;
import library.entities.Loan;
import library.entities.helpers.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ReturnBookControlTest {
    private static Scanner scannerInput;
    private static ILibrary library;
    private static String menuString;
    private static ICalendar calendar;
    private static SimpleDateFormat dateFormat;
    private static LibraryFileHelper libraryHelper;
    private static CalendarFileHelper calendarHelper;

    @BeforeEach
    void setUp() {
        libraryHelper = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
        calendarHelper = new CalendarFileHelper();
        scannerInput = new Scanner(System.in);
        calendar = calendarHelper.loadCalendar();
        library = libraryHelper.loadLibrary();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }
    @Test
    void testFine1DayOverDue() {
        library.addPatron("Smith", "John", "email@test.com", 554466L);
        library.addBook("Tolken", "LOTR", "11223344");
        ILoan loan = library.issueLoan(library.getBookById(1), library.getPatronById(1));
        library.commitLoan(loan);
        calendar.incrementDate(3);
        library.checkCurrentLoansOverDue();

        double actualFine = library.calculateOverDueFine(library.getCurrentLoanByBookId(1));
        double expectedFine = 1.0;

        assertEquals(expectedFine, actualFine);
    }

    @Test
    void testCorrectFineCharged() {
        library.addPatron("Smith", "John", "email@test.com", 554466L);
        library.addBook("Tolken", "LOTR", "11223344");
        ILoan loan = library.issueLoan(library.getBookById(1), library.getPatronById(1));
        library.commitLoan(loan);
        calendar.incrementDate(4); //To be two days overdue
        library.checkCurrentLoansOverDue();

        double actualFine = library.calculateOverDueFine(library.getCurrentLoanByBookId(1));
        double expectedFine = 2.0;

        assertEquals(expectedFine, actualFine);
    }
}